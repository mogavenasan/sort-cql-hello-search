// ==UserScript==
// @name        CQL search for Hello
// @include     https://hello.atlassian.net/wiki/dosearchsite.action?cql=*
// @include     https://hello.atlassian.net/wiki/dosearchsite.action*
// @author      Mogavenasan <mmuthusamy@atlassian.com>, Boon Hui Tan <btan@atlassian.com>, Zulfadli Noor Sazali <znoorsazali@atlassian.com>
// @description CQL search for Hello
// @version     1.0.4
// @namespace   https://hello.atlassian.net/wiki/spaces/~znoorsazali/blog/2019/02/28/406329506/ShipIt+45+-+Getting+the+order+in+CQL
// @downloadURL https://bitbucket.org/mogavenasan/sort-cql-hello-search/raw/master/sort-cql-hello-search.user.js
// @updateURL   https://bitbucket.org/mogavenasan/sort-cql-hello-search/raw/master/sort-cql-hello-search.user.js
// @grant       none
// @noframes
// ==/UserScript==

function gascript(){
    var head1 = document.getElementsByTagName('head')[0],
    script1 = document.createElement('script');
    script1.type = 'text/javascript';
    script1.src = 'https://www.googletagmanager.com/gtag/js?id=UA-136331253-1';
    head1.appendChild(script1);

    var head2 = document.getElementsByTagName('head')[0],
    script2 = document.createElement('script');
    script2.type = 'text/javascript';
    script2.text = `window.dataLayer = window.dataLayer || [];
                    function gtag(){dataLayer.push(arguments);}
                    gtag('js', new Date());

                    gtag('config', 'UA-136331253-1');`;
    head2.appendChild(script2);
}

function buildDiv(sortcreateddesc, sortcreatedasc, sortmodifieddesc, sortmodifiedasc) {
    return `
    <div id='cql-sorting-panel' style="padding-left:118px">
        <a class='aui-button aui-button-link' onclick="gtag('event', 'button-clicked', {'event_category': 'created-desc-button', 'event_label': 'created-desc-button'});" href="${sortcreateddesc}">Created (▼)</a>
        <a class='aui-button aui-button-link' onclick="gtag('event', 'button-clicked', {'event_category': 'created-asc-button', 'event_label': 'created-asc-button'});" href="${sortcreatedasc}">Created (▲)</a>
        <a class='aui-button aui-button-link' onclick="gtag('event', 'button-clicked', {'event_category': 'modified-desc-button', 'event_label': 'modified-desc-button'});" href="${sortmodifieddesc}">Last modified (▼)</a>
        <a class='aui-button aui-button-link' onclick="gtag('event', 'button-clicked', {'event_category': 'modified-asc-button', 'event_label': 'modified-asc-button'});" href="${sortmodifiedasc}">Last modified (▲)</a>
    </div>
    `;
}

function main() {
    var pathname = window.location.href;
    pathname = pathname.replace(/\+order\+by\+lastmodified\+desc|\+order\+by\+created\+desc|\+order\+by\+lastmodified\+asc|\+order\+by\+created\+asc/g, '');
    pathname = pathname.replace(/&startIndex=.*$/g, '')

    var qspath = pathname.match(/https:\/\/hello\.atlassian\.net\/wiki\/dosearchsite\.action\?cql=siteSearch.*&queryString.*/g);

    var sortcreateddesc, sortcreatedasc, sortmodifieddesc, sortmodifiedasc, element

    if (qspath == null) {
        var splitpath1 = pathname.split("&search_id")
        sortcreateddesc = splitpath1[0] + "+order+by+created+desc" + "&search_id" + splitpath1[1];
        sortcreatedasc = splitpath1[0] + "+order+by+created+asc" + "&search_id" + splitpath1[1];
        sortmodifieddesc = splitpath1[0] + "+order+by+lastmodified+desc" + "&search_id" + splitpath1[1];
        sortmodifiedasc = splitpath1[0] + "+order+by+lastmodified+asc" + "&search_id" + splitpath1[1];

        element = document.getElementById("cql-sorting-panel");
        element.innerHTML = buildDiv(sortcreateddesc, sortcreatedasc, sortmodifieddesc, sortmodifiedasc);
    } else if (qspath.length == 1) {
        var splitpath2 = pathname.split("&queryString")
        sortcreateddesc = splitpath2[0] + "+order+by+created+desc" + "&queryString" + splitpath2[1];
        sortcreatedasc = splitpath2[0] + "+order+by+created+asc" + "&queryString" + splitpath2[1];
        sortmodifieddesc = splitpath2[0] + "+order+by+lastmodified+desc" + "&queryString" + splitpath2[1];
        sortmodifiedasc = splitpath2[0] + "+order+by+lastmodified+asc" + "&queryString" + splitpath2[1];

        element = document.getElementById("cql-sorting-panel");
        element.innerHTML = buildDiv(sortcreateddesc, sortcreatedasc, sortmodifieddesc, sortmodifiedasc);

    } else {
        console.log("Something went wrong!")
        }
}

window.addEventListener('load', function() {
    gascript()

    var pathname_ori = window.location.href;
    document.querySelector("#search-form").insertAdjacentHTML('beforeend', buildDiv(pathname_ori, pathname_ori, pathname_ori, pathname_ori));
    main();
},false);

document.querySelector(".search-results-wrapper").addEventListener("DOMSubtreeModified", function(){
    main();
});